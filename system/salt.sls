include:
  - system.base

system_salt_pkg:
  pkg.installed:
    - pkgs:
      - salt-minion
    - require:
      - system_base_pkgs

system_salt_minion_config:
  file.managed:
    - name: /etc/salt/minion
    - source: salt://{{ tpldir }}/files/salt_minion
    - user: root
    - group: root
    - mode: '0644'

system_salt_minon_service:
  service.disabled:
    - name: salt-minion
    - require:
      - system_salt_pkg

system_salt_minion_cachedir_permission:
  file.directory:
    - name: /var/cache/salt/minion
    - mode: '0700'
    - require:
      - system_salt_pkg

system_salt_minion_logdir_permission:
  file.directory:
    - name: /var/log/salt
    - user: root
    - group: root
    - mode: '0700'
    - require:
      - system_salt_pkg
