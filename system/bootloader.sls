include:
  - .base

system_bootloader_pkgs:
  pkg.installed:
    - pkgs:
      - grub
      - grub-efi
      - mkinitfs
      - mkinitfs-doc
      - efibootmgr

{% set CURRENT_SUBVOLUME_TIMESTAMP = salt['cmd.shell']('CURRENT_SUBVOLUME=$(LC_ALL=C btrfs sub show / | LC_ALL=C grep Name: | tr -d \'[:space:]\'); TIMESTAMP_FORMAT=\'[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}_[[:digit:]]{2}-[[:digit:]]{2}-[[:digit:]]{2}\'; echo "$CURRENT_SUBVOLUME" | grep -Eo "$TIMESTAMP_FORMAT"') %}
{% set CURRENT_SUBVOLUME_TIMESTAMP_HUMAN = salt['cmd.shell']('CURRENT_SUBVOLUME_TIMESTAMP=\'' + CURRENT_SUBVOLUME_TIMESTAMP + '\'; echo "${CURRENT_SUBVOLUME_TIMESTAMP:0:10} ${CURRENT_SUBVOLUME_TIMESTAMP:11:2}:${CURRENT_SUBVOLUME_TIMESTAMP:14:2}"') %}
{% set KERNEL_VERSION = salt['cmd.shell']('uname -r') %}

system_bootloader_mkinitfs_conf:
  file.keyvalue:
    - name: /etc/mkinitfs/mkinitfs.conf
    - key_values:
        features: '"ata base btrfs keymap kms mmc nvme scsi usb virtio"'
    - separator: '='
    - uncomment: '# '
    - key_ignore_case: false
    - value_ignore_case: false
    - append_if_not_found: true
    - require:
      - system_base_pkgs
      - system_bootloader_pkgs

system_bootloader_os-release:
  file.managed:
    - name: /etc/os-release
    - source: salt://{{ tpldir }}/files/bootloader_os-release.jinja
    - template: jinja
    - context:
        OS_VERSION: {{ salt['cmd.shell']('date "+%Y%m"') }}
        OS_RELEASE_INFO: '"Timestamp: {{ CURRENT_SUBVOLUME_TIMESTAMP_HUMAN }} | Kernel: {{ KERNEL_VERSION }}"'
    - user: root
    - group: root
    - mode: '0644'
