system_base_pkgs:
  pkg.installed:
    - pkgs:
      - linux-lts
      - linux-firmware-none
      - openjdk17-jre-headless
      - jq
      - vim
      - git
      - findmnt
      - util-linux
      - sudo

system_base_bin_dir:
  file.directory:
    - name: /usr/local/noveria/bin
    - makedirs: true
    - user: root
    - group: root
    - dir_mode: '0755'
    - file_mode: '0644'

system_base_apps_dir:
  file.directory:
    - name: /usr/local/noveria/apps
    - makedirs: true
    - user: root
    - group: root
    - dir_mode: '0755'
    - file_mode: '0644'

system_base_modules_service:
  service.running:
    - name: modules
    - enable: True

# TUN permission fix service workaround
system_base_tun-perm_service:
  file.managed:
    - name: /etc/init.d/tun-perm
    - source: salt://{{ tpldir }}/files/base_tun-perm.initd
    - user: root
    - group: root
    - mode: '0755'
  service.enabled:
    - name: tun-perm
    - require:
      - system_base_modules_service
