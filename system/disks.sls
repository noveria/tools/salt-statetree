system_disks_pkgs:
  pkg.installed:
    - pkgs:
      - btrfs-progs

system_disks_btrfs_module:
  file.append:
    - name: /etc/modules
    - text: "btrfs"
    - require:
      - system_base_pkgs

system_disks_btrfs_mount_permission:
  file.directory:
    - name: /btrfs
    - user: root
    - group: root
    - dir_mode: '0755'

{% set ROOT_UUID = salt['cmd.shell']('lsblk -o LABEL,UUID | grep ROOT | awk \'{print $2}\'') %}
{% set ESP_UUID = salt['cmd.shell']('lsblk -o LABEL,UUID | grep EFI | awk \'{print $2}\'') %}

system_disks_fstab:
  file.managed:
    - name: /etc/fstab
    - source: salt://{{ tpldir }}/files/disks_fstab.jinja
    - template: jinja
    - context:
        TIMESTAMP: {{ salt['pillar.get']('timestamp') }}
        ROOT_UUID: {{ ROOT_UUID }}
        ESP_UUID: {{ ESP_UUID }}
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - system_disks_btrfs_mount_permission
