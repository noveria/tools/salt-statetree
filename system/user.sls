system_user_noveria_create:
  user.present:
    - name: noveria
    - shell: /bin/false
    - home: /home/noveria
    - usergroup: True
    - createhome: True

system_user_root_edit:
  user.present:
    - name: root
    - shell: /bin/zsh