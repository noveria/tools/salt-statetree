include:
  - system.base

{% from "map.jinja" import noveria %}

system_shell_pkgs:
  pkg.installed:
    - pkgs:
      - zsh
      - alpine-zsh-config
      - bash
    - require:
      - system_base_pkgs

system_shell_profile:
  file.managed:
    - name: /etc/profile
    - source: salt://{{ tpldir }}/files/shell_profile.jinja
    - template: jinja
    - context:
        CUSTOM_BIN: {{ noveria.bin_dir }}
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - system_shell_pkgs
