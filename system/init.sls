include:
  - .base
  - .bootloader
  - .disks
  - .user
  - .shell
  - .salt
  - .candy