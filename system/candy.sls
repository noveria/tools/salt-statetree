include:
  - .bootloader

system_candy_motd:
  file.managed:
    - name: /etc/motd
    - source: salt://{{ tpldir }}/files/candy_motd.jinja
    - template: jinja
    - context:
        OS_NAME: {{ salt['cmd.shell']('grep -E ^PRETTY_NAME /etc/os-release | cut -d= -f2') }}
        OS_RELEASE_INFO: {{ salt['cmd.shell']('grep -E ^RELEASE_INFO /etc/os-release | cut -d= -f2') }}
        OS_ISSUE_URL: {{ salt['cmd.shell']('grep -E ^BUG_REPORT_URL /etc/os-release | cut -d= -f2') }}
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - system_bootloader_os-release