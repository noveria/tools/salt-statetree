include:
  - .pkg

openssh_config_configuration:
  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://{{ tpldir }}/files/openssh_sshd_config

openssh_config_keys:
  file.managed:
    - name: /root/.ssh/authorized_keys
    - source: salt://{{ tpldir }}/files/openssh_authorized_keys
    - makedirs: True
    - user: root
    - group: root
    - require:
      - openssh_pkg
