include:
  - .config

openssh_service_enable:
  service.running:
    - name: sshd
    - enable: True
    - watch:
      - openssh_config_configuration
      - openssh_config_keys
    - require:
      - openssh_config_configuration
      - openssh_config_keys
