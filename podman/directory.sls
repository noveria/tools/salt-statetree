include:
  - .user

podman_directory_permissions:
  file.directory:
    - name: /opt/podman
    - user: puser
    - group: puser
    - dir_mode: '0755'
    - file_mode: '0644'
    - require:
      - podman_user_puser
