{% from tpldir+"/map.jinja" import containers %}

include:
  - podman.directory

podman_containers_npm_compose:
  file.managed:
    - name: {{ containers.podman_dir }}/npm/docker-compose.yml
    - source: salt://{{ tpldir }}/files/npm_docker-compose.yml
    - user: puser
    - group: puser
    - mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions

podman_containers_npm_data_dir:
  file.directory:
    - name: {{ containers.podman_dir }}/npm/data
    - user: puser
    - group: puser
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions

podman_containers_npm_letsencrypt_dir:
  file.directory:
    - name: {{ containers.podman_dir }}/npm/letsencrypt
    - user: puser
    - group: puser
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions
