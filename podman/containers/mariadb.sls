{% from tpldir+"/map.jinja" import containers %}

include:
  - podman.directory

podman_containers_mariadb_compose:
  file.managed:
    - name: {{ containers.podman_dir }}/mariadb/docker-compose.yml
    - source: salt://{{ tpldir }}/files/mariadb_docker-compose.yml.jinja
    - template: jinja
    - context:
        MARIADB_ROOT_PASSWORD: {{ salt['pillar.get']('podman:containers:mariadb:rootpwd') }}
        MARIADB_USER: {{ salt['pillar.get']('podman:containers:mariadb:user') }}
        MARIADB_USER_PASSWORD: {{ salt['pillar.get']('podman:containers:mariadb:userpwd') }}
    - user: puser
    - group: puser
    - mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions

podman_containers_mariadb_data_dir:
  file.directory:
    - name: {{ containers.podman_dir }}/mariadb/data
    - user: puser
    - group: puser
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions
