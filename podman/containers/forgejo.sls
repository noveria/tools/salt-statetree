{% from tpldir+"/map.jinja" import containers %}

include:
  - podman.directory

podman_containers_forgejo_compose:
  file.managed:
    - name: {{ containers.podman_dir }}/forgejo/docker-compose.yml
    - source: salt://{{ tpldir }}/files/forgejo_docker-compose.yml.jinja
    - template: jinja
    - context:
        MARIADB_USER: {{ salt['pillar.get']('podman:containers:mariadb:user') }}
        MARIADB_USER_PASSWORD: {{ salt['pillar.get']('podman:containers:mariadb:userpwd') }}
    - user: puser
    - group: puser
    - mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions

podman_containers_forgejo_data_dir:
  file.directory:
    - name: {{ containers.podman_dir }}/forgejo/data
    - user: puser
    - group: puser
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - require:
      - podman_directory_permissions
