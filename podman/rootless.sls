include:
  - .pkg
  - .user

podman_rootless_cgroup_mode:
  file.keyvalue:
    - name: /etc/rc.conf
    - key_values:
        rc_cgroup_mode: '"unified"'
    - separator: '='
    - uncomment: '#'
    - key_ignore_case: false
    - value_ignore_case: false
    - append_if_not_found: true
    - require:
      - podman_pkg

podman_rootless_cgroups:
  service.running:
    - name: cgroups
    - enable: True
    - watch:
      - podman_rootless_cgroup_mode

podman_rootless_modules:
  file.append:
    - name: /etc/modules
    - text:
      - tun
      - fuse
      - ip_tables

podman_rootless_subgid:
  file.append:
    - name: /etc/subgid
    - text: puser:100000:65536
    - require:
      - podman_user_puser

podman_rootless_subuid:
  file.append:
    - name: /etc/subuid
    - text: puser:100000:65536
    - require:
      - podman_user_puser
