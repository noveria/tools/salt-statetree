include:
  - .rootless

podman_service:
  service.running:
    - name: podman
    - enable: True

podman_service_unprivileged_ports:
  file.managed:
    - name: /etc/sysctl.d/podman.conf
    - source: salt://{{ tpldir }}/files/files_unprivileged_ports
    - user: root
    - group: root
    - mode: '0644'
