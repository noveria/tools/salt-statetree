include:
  - .pkg

podman_user_puser:
  user.present:
    - name: puser
    - uid: 2000
    - shell: /bin/zsh
    - home: /home/puser
    - usergroup: True
    - createhome: True
    - require:
      - podman_pkg

podman_user_openssh_keys:
  file.managed:
    - name: /home/puser/.ssh/authorized_keys
    - source: salt:///openssh/files/openssh_authorized_keys
    - makedirs: True
    - user: puser
    - group: puser
    - require:
      - podman_user_puser
