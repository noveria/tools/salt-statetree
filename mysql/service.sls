include:
  - .directory

{% if not salt['file.directory_exists']('/var/lib/mysql/mysql') %}
mysql_service_setup:
  cmd.run:
    - name: /etc/init.d/mariadb setup
    - runas: root
    - shell: /bin/ash
    - require:
      - mysql_directory

mysql_service_enable:
  service.enabled:
    - name: mariadb
    - require:
      - mysql_service_setup
{% endif %}
