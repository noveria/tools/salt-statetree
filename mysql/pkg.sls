mysql_pkg:
  pkg.installed:
    - pkgs:
      - mariadb
      - mariadb-client
      - mariadb-openrc
