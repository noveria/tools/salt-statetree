include:
  - .pkg

mysql_directory:
  file.directory:
    - name: /var/lib/mysql
    - create: False
    - replace: False
    - user: mysql
    - group: mysql
    - recurse:
      - user
      - group
    - require:
      - mysql_pkg
