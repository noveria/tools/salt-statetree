# Noveria Salt-State

![SaltStack® Logo](https://upload.wikimedia.org/wikipedia/commons/6/64/SaltStack_logo_blk_2k.png)

This repository is for the master-less salt-statetree for the noveria host, running Alpine Linux

It managed the complete system using the saltproject.

For further instructions, head to the official salt docs: https://docs.saltproject.io/en/latest/contents.html
