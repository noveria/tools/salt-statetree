include:
  - system.base

{% from "map.jinja" import noveria %}

util_noveriablcgen_git:
  git.latest:
    - name: https://gitlab.com/noveria/tools/noveriablcgen.git
    - target: {{ noveria.app_dir }}/noveriablcgen
    - user: root
    - force_reset: True
    - require:
      - system_base_apps_dir

util_noveriablcgen_link:
  file.symlink:
    - name: {{ noveria.bin_dir }}/noveriablcgen
    - target: {{ noveria.app_dir }}/noveriablcgen/noveriablcgen
    - user: root
    - group: root
    - mode: '0755'
    - require:
      - system_base_bin_dir
      - util_noveriablcgen_git

util_noveriablcgen_config:
  file.managed:
    - name: {{ noveria.etc_dir }}/noveriablcgen/noveriablcgen.json
    - source: salt://{{ tpldir }}/files/noveriablcgen_config.json.jinja
    - template: jinja
    - context:
        BTRFS_ROOT: "/btrfs"
        GRUB_CONFD: "/etc/grub.d"
        GRUB_CONFIG_FILENAME: "10_noveria"
        ROOT_UUID: {{ salt['cmd.shell']('lsblk -o LABEL,UUID | grep ROOT | awk \'{print $2}\'') }}
        GRUB_CONFIG: "/boot/grub/grub.cfg"
    - mode: '0644'
    - makedirs: true
    - user: root
    - group: root
    - require:
      - util_noveriablcgen_git