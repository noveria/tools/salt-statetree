include:
  - system.base
  - .firewall

network_interfaces:
  file.managed:
    - name: /etc/network/interfaces
    - source: salt://{{ tpldir }}/files/network_interfaces
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - system_base_pkgs

network_service_reload:
  service.running:
    - name: networking
    - enable: true
    - reload: true
    - watch:
      - network_interfaces