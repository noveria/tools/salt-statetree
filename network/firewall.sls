network_firewall_pkgs:
  pkg.installed:
    - pkgs:
      - nftables

network_firewall_rules:
  file.managed:
    - name: /etc/nftables.d/noveria.nft
    - source: salt://{{ tpldir }}/files/firewall_rules.nft.jinja
    - template: jinja
    - context:
        STATE: {{ sls }}
        ALLOWED_PORTS: [443, 25565]
    - user: root
    - group: root
    - mode: '0600'
    - makedirs: true
    - require:
      - network_firewall_pkgs

network_firewall_service_reload:
  service.running:
    - name: nftables
    - enable: true
    - reload: true
    - watch:
      - network_firewall_rules
    - require:
      - network_firewall_pkgs
  
